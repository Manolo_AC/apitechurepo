package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * Created by U501257 on 25/05/2021.
 */

@RestController
public class ProductController {
    static final String APIBaseURL = "/apitechu/v1";

    @GetMapping(APIBaseURL + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");


        return ApitechuApplication.productModels;

    }

    @GetMapping(APIBaseURL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("Id es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                result = product;
            }
        }

        return result;

    }

    @PostMapping(APIBaseURL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del nuevo producto es " + newProduct.getId());
        System.out.println("La descripción del nuevo producto es " + newProduct.getDesc());
        System.out.println("El precio del nuevo producto es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;

    }

    @PutMapping(APIBaseURL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en parámetro URL es " + id);
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;

    }

    @DeleteMapping(APIBaseURL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto para borrar encontrado");
                foundProduct = true;
                result = product;
            }
        }

        if (foundProduct == true) {
            System.out.println("Borrando producto");
            ApitechuApplication.productModels.remove(result);
        }

        return new ProductModel();

    }

    @PatchMapping(APIBaseURL + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en parámetro URL es " + id);
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                product = productInList;
            }

            if (productInList.getDesc() != null) {
                System.out.println("La descripción del producto a actualizar es " + product.getDesc());
                productInList.setDesc(product.getDesc());

            }
            if (productInList.getPrice() > 0) {
                System.out.println("El precio del producto a actualizar es " + product.getPrice());
                productInList.setPrice(product.getPrice());

            }
        }

        return product;

    }
}
