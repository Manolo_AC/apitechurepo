package com.techu.apitechu;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by U501257 on 24/05/2021.
 */
@RestController
public class HelloController {

    @RequestMapping("/")
    public String index(){
        return "Hola Mundo desde API TechU!";

    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "Tech U") String name){
        return String.format("Hola %s!", name);
    }
}
